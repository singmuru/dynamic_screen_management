import dotenv from "dotenv";
import express from "express";
import bodyParser from 'body-parser';
import router from './routes'
import { StatusCodes } from 'http-status-codes';

// initialize configuration
dotenv.config();

const port = process.env.SERVER_PORT; // default port to listen

const app = express();

app.use(bodyParser.json());

app.use('/', router);


app.use((req, res, next) => {
    const err: any = new Error(`${req.method} ${req.url} Not Found`);
    err.status = StatusCodes.NOT_FOUND;
    next(err);
});

app.use((err: any, req: any, res: any, next: any) => {
    res.status(err.status || StatusCodes.INTERNAL_SERVER_ERROR);
    res.json({
        error: {
            message: err.message,
        },
    });
});

// start the Express server
app.listen(port, () => {
    // console.log(`server started at http://localhost:${port}`);
});