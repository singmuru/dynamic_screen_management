import express from 'express';
import fs from 'fs';
import path from 'path';
import { StatusCodes } from 'http-status-codes';

const router = express.Router();

const getMetaData = async (req: any, res: any, next: any) => {
    try {
        let { module_name, screen_name } = req.query;
        if (!module_name || !screen_name) {
            module_name = req.body.module_name;
            screen_name = req.body.screen_name;
        }
        if (!module_name || !screen_name) {
            module_name = req.params.module_name;
            screen_name = req.params.screen_name;
        }
        if (!module_name || !screen_name) {
            res.status(StatusCodes.NOT_FOUND).json({
                "error": {
                    "message": "No Module Name or Screen Name found"
                }
            })
            return
        }

        const filesList = getAllFiles(__dirname);
        const filteredList = filesList.filter(file => file.includes(module_name)
        )
        if (filteredList.length > 0) {
            const filteredListByScreenName = filteredList.filter(file => file.includes(screen_name)
            )
            if (filteredListByScreenName.length > 0 && filteredListByScreenName[0]) {
                const readFileData = fs.readFileSync(filteredListByScreenName[0]);
                if (readFileData.toString() === "") {
                    res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
                        "error": {
                            "message": "file data is empty.Could please check the file in server"
                        }
                    })
                }
                res.status(StatusCodes.OK).json(JSON.parse(readFileData.toString()));
            } else {
                res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
                    "error": {
                        "message": "No file there with screen name.Kindly check the screen name"
                    }
                })
            }
        } else {
            res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
                "error": {
                    "message": "No Directory there with module name.Kindly check the module name"
                }
            })
        }
    } catch (exception) {
        next(exception);
    }
}

/**
 * Find all files inside a dir, recursively.
 * @function getAllFiles
 * @param  {string} dir Dir path string.
 * @return {string[]} Array with all file names that are inside the directory.
 */
const getAllFiles = (dirnamePath: string): string[] =>
    fs.readdirSync(dirnamePath).reduce((files, file) => {
        const name = path.join(dirnamePath, file);
        const isDirectory = fs.statSync(name).isDirectory();
        return isDirectory ? [...files, ...getAllFiles(name)] : [...files, name];
    }, []);

router.route('/v1/api/get-meta-data').get(getMetaData)
router.route('/v1/api/get-meta-data/:module_name/:screen_name/').get(getMetaData)

export default router;



