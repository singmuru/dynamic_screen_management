"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const routes_1 = __importDefault(require("./routes"));
const http_status_codes_1 = require("http-status-codes");
// initialize configuration
dotenv_1.default.config();
const port = process.env.SERVER_PORT; // default port to listen
const app = express_1.default();
app.use(body_parser_1.default.json());
app.use('/', routes_1.default);
app.use((req, res, next) => {
    const err = new Error(`${req.method} ${req.url} Not Found`);
    err.status = http_status_codes_1.StatusCodes.NOT_FOUND;
    next(err);
});
app.use((err, req, res, next) => {
    res.status(err.status || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR);
    res.json({
        error: {
            message: err.message,
        },
    });
});
// start the Express server
app.listen(port, () => {
    // console.log(`server started at http://localhost:${port}`);
});
//# sourceMappingURL=index.js.map