"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const http_status_codes_1 = require("http-status-codes");
const router = express_1.default.Router();
const getMetaData = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { module_name, screen_name } = req.query;
        if (!module_name || !screen_name) {
            module_name = req.body.module_name;
            screen_name = req.body.screen_name;
        }
        if (!module_name || !screen_name) {
            module_name = req.params.module_name;
            screen_name = req.params.screen_name;
        }
        if (!module_name || !screen_name) {
            res.status(http_status_codes_1.StatusCodes.NOT_FOUND).json({
                "error": {
                    "message": "No Module Name or Screen Name found"
                }
            });
            return;
        }
        const filesList = getAllFiles(__dirname);
        const filteredList = filesList.filter(file => file.includes(module_name));
        if (filteredList.length > 0) {
            const filteredListByScreenName = filteredList.filter(file => file.includes(screen_name));
            if (filteredListByScreenName.length > 0 && filteredListByScreenName[0]) {
                const readFileData = fs_1.default.readFileSync(filteredListByScreenName[0]);
                if (readFileData.toString() === "") {
                    res.status(http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR).json({
                        "error": {
                            "message": "file data is empty.Could please check the file in server"
                        }
                    });
                }
                res.status(http_status_codes_1.StatusCodes.OK).json(JSON.parse(readFileData.toString()));
            }
            else {
                res.status(http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR).json({
                    "error": {
                        "message": "No file there with screen name.Kindly check the screen name"
                    }
                });
            }
        }
        else {
            res.status(http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR).json({
                "error": {
                    "message": "No Directory there with module name.Kindly check the module name"
                }
            });
        }
    }
    catch (exception) {
        next(exception);
    }
});
/**
 * Find all files inside a dir, recursively.
 * @function getAllFiles
 * @param  {string} dir Dir path string.
 * @return {string[]} Array with all file names that are inside the directory.
 */
const getAllFiles = (dirnamePath) => fs_1.default.readdirSync(dirnamePath).reduce((files, file) => {
    const name = path_1.default.join(dirnamePath, file);
    const isDirectory = fs_1.default.statSync(name).isDirectory();
    return isDirectory ? [...files, ...getAllFiles(name)] : [...files, name];
}, []);
router.route('/v1/api/get-meta-data').get(getMetaData);
router.route('/v1/api/get-meta-data/:module_name/:screen_name/').get(getMetaData);
exports.default = router;
//# sourceMappingURL=routes.js.map